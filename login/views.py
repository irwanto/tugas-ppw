from django.shortcuts import render
from .forms import Login_Form
from django.http import HttpResponseRedirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout

def submit(request):
    form = Login_Form(request.POST)
    if request.method == 'POST' and form.is_valid():
        response = {}
        response['email'] = request.POST['email']
        response['password'] = request.POST['new_password']
        return render(request, 'doneregis.html', response)
    else:
        return render(request, 'login.html', {'regisform': form})

def loginnya(request):
    return render(request, "login.html", {'regisform' : Login_Form})

def logoutPage(request):
	request.session.flush()
	logout(request)
	return HttpResponseRedirect('/login')
