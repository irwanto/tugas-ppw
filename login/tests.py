from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.apps import apps
from .apps import LoginConfig
from .views import loginnya
from django.http import HttpRequest
import unittest

class StoryUnitTest(TestCase):
    def test_login_page_is_exist(self):
        response = Client().get('/login')
        self.assertEqual(response.status_code,301)
        
    def test_apps(self):
        self.assertEqual(LoginConfig.name, 'login')
        self.assertEqual(apps.get_app_config('login').name, 'login')


# Create your tests here.
