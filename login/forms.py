from django import forms

class Login_Form(forms.Form):

    attrsemail = {
        'class' : 'form-control',
        'placeholder' : 'Masukan emailmu'
    }

    attrspassword = {
        'class' : 'form-control',
        'placeholder' : 'Masukan password',
        'type' : 'password'
    }

    email = forms.EmailField(label='Email', required=True, max_length=30, widget=forms.TextInput(attrs=attrsemail))
    new_password = forms.CharField(label='Password', required=True, max_length=30, widget=forms.TextInput(attrs=attrspassword))
