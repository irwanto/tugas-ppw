from django.urls import path
from .views import loginnya, logoutPage

app_name="Login"
urlpatterns = [
    path('', loginnya, name='login'),
    path('byebye/', logoutPage, name="logout")
]
