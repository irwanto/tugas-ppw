from django.db import models 


class About(models.Model): 
    name = models.CharField(max_length=10)
    testimony = models.TextField(max_length=400)