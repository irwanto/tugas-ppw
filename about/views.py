from django.shortcuts import render, reverse
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse 
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse
from .models import About 
import urllib.request, json, requests

def indexAbout(request):
    all_about = About.objects.all()
    return render(request, 'about.html', {'about': all_about})

def indexTestimoni(request):
    if request.method == 'POST':
        name = request.POST['name']
        testimony = request.POST['testimony']
        About.objects.create(name=name, testimony=testimony)
        return JsonResponse({'saved': True, 'name': name, 'testimony': testimony})
    return JsonResponse({'saved': False})

       