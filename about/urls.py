from django.urls import path
from .views import *


app_name='About'
urlpatterns = [
    path('', indexAbout, name='indexAbout'),
    path('testimoni/', indexTestimoni, name='indexTestimoni'),
]