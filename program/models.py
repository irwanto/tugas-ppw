from django.db import models

# Create your models here.
class Program(models.Model):
	link_img = models.URLField()
	judul = models.CharField(max_length=25)
	deskripsi = models.CharField(max_length=100)
	kategori = models.CharField(max_length=15)
	jumlah = models.IntegerField()
	lokasi = models.CharField(max_length=15)

# https://ibb.co/d0yp6L
# https://ibb.co/hG8BLf
# https://ibb.co/cN9J0f
