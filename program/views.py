from django.shortcuts import render
from .models import *

# Create your views here.
def program(request):
	programs = Program.objects.all()
	if request.user.is_authenticated:
		request.session['first_name'] = request.user.first_name
		request.session['last_name'] = request.user.last_name
		request.session['email'] = request.user.email
	
	response={
		'programs' : programs
	}
	return render(request,'program.html',response)