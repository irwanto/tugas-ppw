from django.test import TestCase
from .views import *
from .models import *
from django.test import Client
from django.urls import resolve
import unittest

# Create your tests here.
class ProgramTest(TestCase):
	def test_landing_page_url_is_exist(self):
		response = Client().get('/program/')
		self.assertEqual(response.status_code,200)

	def test_landing_page_using_landing_page_function(self):
		found = resolve('/program/')
		self.assertEqual(found.func, program)

	def test_landing_page_using_landing_page_template(self):
		response = Client().get('/program/')
		self.assertTemplateUsed(response,'program.html')

	def test_html_contain_brand(self):
		response = Client().get('/program/')
		html_response = response.content.decode('utf8')
		self.assertIn('SINARPERAK',html_response)