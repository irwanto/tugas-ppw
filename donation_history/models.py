from django.db import models
from django.contrib.auth.models import User
from program.models import Program

# Create your models here.
class DonationHistory(models.Model):
    program = models.ForeignKey(Program, on_delete=models.CASCADE, default=None)
    name = models.CharField(max_length=100)
    email = models.EmailField()
    jumlah_donasi = models.IntegerField(default=0)

