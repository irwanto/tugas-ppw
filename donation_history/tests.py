from django.test import TestCase
from .views import *
from .models import *
from program.models import *
from django.test import Client
from django.urls import resolve
import unittest

# Create your tests here.
class HistoryTest(TestCase):
	def setUp(self):
		user = User.objects.create_superuser(username="admin",email="admin@mail.com",password="admin")
		self.user = authenticate(username='admin', password='admin')

		self.client = Client()
		self.logged_in = self.client.login(username = self.user.username, password = "admin")

	def test_list_page_url_redirect_if_user_not_logged_in(self):
		response = Client().get('/list_donasi/')
		self.assertEqual(response.status_code,302)	# redirect to login

	def test_list_page_url_is_exist(self):
		response = self.client.get('/list_donasi/')
		self.assertEqual(response.status_code,200)

	def test_list_page_using_donationView_function(self):
		found = resolve('/list_donasi/')
		self.assertEqual(found.func, donationView)

	def test_list_page_using_list_page_template(self):
		response = self.client.get('/list_donasi/')
		self.assertTemplateUsed(response,'history.html')
	
	def test_donation_data_url_redirect_if_user_not_logged_in(self):
		response = Client().get('/list_donasi/data/')
		self.assertEqual(response.status_code,302)	# redirect to login

	def test_donation_data_JSON_response(self):
		response = self.client.get('/list_donasi/data/')
		self.assertEqual(response.status_code, 200)
		self.assertJSONEqual(str(response.content, encoding='utf8'), []) #user not donate yet so JSON still empty