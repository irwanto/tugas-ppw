from django.urls import path
from .views import *
#di url project udah manggil news. jadi di url app gausah manggil lagi
app_name='donation_history'
urlpatterns = [
    path('', donationView, name='donations'),
    path('data/', donation_data, name='donation_data'),
]
