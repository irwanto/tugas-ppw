from django.contrib import admin
from .models import DonationHistory

admin.site.register(DonationHistory)

# Register your models here.
