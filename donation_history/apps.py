from django.apps import AppConfig


class DonationHistoryConfig(AppConfig):
    name = 'donation_history'
