from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from .models import *
from program.models import *

# Create your views here.
def donationView(request):
    if request.user.is_authenticated:
    	return render(request,'history.html')
    else:
    	notif = "Harap registrasi terlebih dahulu agar dapat melihat list donasi"
    	messages.warning(request, notif)
    	return HttpResponseRedirect('/login/')

def donation_data(request):
	if request.user.is_authenticated:
		donations = DonationHistory.objects.filter(email = request.user.email).values()
		donation_list = list(donations)
		for donation in donation_list:
 			donation['program'] = Program.objects.get(id = donation['program_id']).judul
		return JsonResponse(donation_list, safe = False)
	else:
		notif = "Harap registrasi terlebih dahulu agar dapat melihat list donasi"
		messages.warning(request, notif)
		return HttpResponseRedirect('/login/')