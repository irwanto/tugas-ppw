# Generated by Django 2.1.1 on 2018-10-17 13:08

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('program', '0005_program_jumlah'),
        ('Donasi', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='donasi',
            name='program',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='program.Program'),
        ),
    ]
