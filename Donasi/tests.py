from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import view_form_donasi, buat_donasi
from .models import Donasi
from Donatur.models import Donatur
from program.models import Program
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User

class UnitTest(TestCase):
	def setUp(self):
		self.program = Program.objects.create(link_img='', judul='program_tes', deskripsi='testing', kategori='bencana', jumlah=0, lokasi='dimana')

		user = User.objects.create_superuser(username="admin",email="admin@mail.com",password="admin", first_name="first", last_name="last")
		self.user = authenticate(username='admin', password='admin')

		self.client = Client()
		self.logged_in = self.client.login(username = self.user.username, password = "admin")

		self.session = self.client.session
		self.session['first_name'] = user.first_name
		self.session['last_name'] = user.last_name
		self.session['email'] = user.email
		self.session.save()

	def test_donasi_page_redirect_code_if_user_not_logged_in(self):
		response = Client().get('/donasi/')
		self.assertEqual(response.status_code,302)

	def test_donasi_page_if_user_logged_in(self):
		response = self.client.get('/donasi/')
		self.assertEqual(response.status_code, 200)

	def test_donasi_page_using_form_donasi_template_if_logged_in(self):
	 	response = self.client.get('/donasi/')
	 	self.assertTemplateUsed(response, 'form_donasi.html')

	def test_donasi_page_using_view_form_donasi_function(self):
		found = resolve('/donasi/')
		self.assertEqual(found.func, view_form_donasi)

	def test_form_donasi_not_valid(self):
		response_post = self.client.post('/donasi/buat_donasi', {'jumlah_donasi': '100', 'donasi_sebagai_anonim': False})
		self.assertEqual(response_post.status_code, 200)

	def test_form_donasi_post_success(self):
	 	response_post = self.client.post('/donasi/buat_donasi', {'nama_program': 'program_tes', 'jumlah_donasi': '100', 'donasi_sebagai_anonim': False})
	 	self.assertEqual(response_post.status_code, 302)
	 	self.assertEqual(response_post.url, '/donasi/donasi_berhasil')

	def test_donasi_sebagai_anonim(self):
		response_post = self.client.post('/donasi/buat_donasi', {'nama_program': 'program_tes', 'jumlah_donasi': '100', 'donasi_sebagai_anonim': True})
		self.assertEqual(response_post.status_code, 302)
		self.assertEqual(response_post.url, '/donasi/donasi_berhasil')

		status_count = Donasi.objects.all().count()
		self.assertEqual(status_count, 1)

		donasi = Donasi.objects.get(program=self.program)
		self.assertEqual(donasi.donatur, 'Anonim')

	def test_donasi_berhasil_page(self):
		response = Client().get('/donasi/donasi_berhasil')
		self.assertTemplateUsed(response, 'donasi_berhasil.html')

	def test_donasi_model(self):
		donasi = Donasi(donatur="test", program=self.program, jumlah_donasi=100)
		self.assertEqual(str(donasi), "Donasi test kepada program_tes")
