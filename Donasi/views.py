from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import FormDonasi
from .models import Donasi
from program.models import Program
from Donatur.models import Donatur
from donation_history.models import DonationHistory
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core import serializers
from django.http import JsonResponse, HttpResponse
import json

def view_form_donasi(request):
    if request.user.is_authenticated:
        form = FormDonasi()
        return render(request, 'form_donasi.html', {'form': form})
    else:
        notif = "Harap registrasi terlebih dahulu agar dapat melakukan donasi"
        messages.warning(request, notif)
        return HttpResponseRedirect('/login/')

@login_required
def buat_donasi(request):
    form = FormDonasi(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        nama_program = request.POST['nama_program']
        nama_donatur = request.session['first_name'] + " " + request.session['last_name']
        email = request.session['email']
        jumlah_donasi = request.POST['jumlah_donasi']
        donasi_sebagai_anonim = request.POST.get("donasi_sebagai_anonim")

        if donasi_sebagai_anonim:
            nama_donatur = 'Anonim'

        program = Program.objects.get(judul=nama_program)
        program.jumlah += int(jumlah_donasi)
        program.save()

        new_donasi = Donasi(donatur=nama_donatur, program=program, jumlah_donasi=int(jumlah_donasi))
        new_donasi.save()

        list_donasi = DonationHistory(program = program, name=nama_donatur, email = email, jumlah_donasi = jumlah_donasi)
        list_donasi.save()

        data = model_to_dict(list_donasi)

        return HttpResponseRedirect('/donasi/donasi_berhasil')
    return render(request, 'form_donasi.html', {'form': form})

def donasi_berhasil(request):
    return render(request, 'donasi_berhasil.html', {})

def model_to_dict(obj):
    data =  serializers.serialize('json',[obj,])
    struct = json.loads(data)
    data = json.dumps(struct[0]['fields'])
    return data
