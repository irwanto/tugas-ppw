from django import forms
from program.models import Program
from Donatur.models import Donatur

class MyModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.judul

class FormDonasi(forms.Form):
    program_attrs = {
        'class' : 'form-control',
    }

    donatur_attrs = {
        'class' : 'form-control',
        'placeholder': 'Nama lengkap'
    }

    email_attrs = {
        'class' : 'form-control',
        'placeholder' : 'Email'
    }

    jumlah_attrs = {
        'class' : 'form-control',
        'placeholder' : '0'
    }

    nama_program = MyModelChoiceField(required=True, queryset=Program.objects.all(), empty_label='Pilih program', to_field_name='judul', widget=forms.Select(attrs=program_attrs))
    # nama_donatur = forms.CharField(required=True, widget=forms.TextInput(attrs=donatur_attrs))
    # email = forms.EmailField(required=True, widget=forms.EmailInput(attrs=email_attrs))
    jumlah_donasi = forms.DecimalField(required=True, widget=forms.NumberInput(attrs=jumlah_attrs))
    donasi_sebagai_anonim = forms.BooleanField(required=False, widget=forms.CheckboxInput())

    # def clean_email(self):
    #     email = self.cleaned_data.get('email')
    #     if (Donatur.objects.filter(email=email).exists()):
    #         return email
    #     else:
    #         raise forms.ValidationError('Email tidak terdaftar')
