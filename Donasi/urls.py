from django.urls import path
from . import views

app_name='Donasi'
urlpatterns = [
    path('', views.view_form_donasi, name="view_form_donasi"),
    path('buat_donasi', views.buat_donasi, name="buat_donasi"),
    path('donasi_berhasil', views.donasi_berhasil, name="donasi_berhasil"),
]
