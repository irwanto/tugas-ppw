from django.db import models
from Donatur.models import Donatur
from program.models import Program

class Donasi(models.Model):
    donatur = models.CharField(max_length=100)
    program = models.ForeignKey(Program, on_delete=models.CASCADE, default=None)
    jumlah_donasi = models.IntegerField(default=0)

    def __str__(self):
        return "Donasi " + self.donatur + " kepada " + self.program.judul
