function testimoni() {
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var name = $('#id_name').val();
    var testimony = $('#id_testimony').val()
    $.ajax({
        method: "POST",
        url: "/about/testimoni/",
        headers: {
            "X-CSRFToken": csrftoken,
        },
        data: {
            name: name,
            testimony: testimony
        },
        success: function (result) {
            if (result.saved) {
                var div1 = '<div class="card">';
                var div2 = '<div id="main" style="display:-webkit-flex;justify-content:center;">';
                var div3 = '<div class="card-body berita-bg" style = "background-color: #3C444D;">';
                var nama = result.name;
                var testi = result.testimony;
                var tes = div1+div2+div3+'<h5 class="card-title berita-text" style="color:white;">'+testi+'</h5><p class="card-text berita-text" style="color:white; text-align: right;" >'+nama+'</p></div></div></div>';
                $('#hasil_testi').prepend(tes);
                $('#id_testimony').val('');
            }

        },
    })
}
