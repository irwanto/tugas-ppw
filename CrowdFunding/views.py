from django.shortcuts import render

def show_hello_world(request):
    return render(request, 'HelloWorld.html', {})
