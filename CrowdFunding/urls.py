from django.contrib import admin
from django.urls import path, include
from . import views

app_name='CrowdFunding'
urlpatterns = [
    path('', views.show_hello_world, name='show_hello_world'),
    path('admin/', admin.site.urls),
    path('registrasi', include('Donatur.urls'))
]
