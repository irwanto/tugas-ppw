from django.urls import path
from .views import *
#di url project udah manggil news. jadi di url app gausah manggil lagi
app_name='news'
urlpatterns = [
    path('', indexNews, name='indexNews'),
    path('berita1/', berita1, name='berita1'),
    path('berita2/', berita2, name='berita2'),
    path('berita3/', berita3, name='berita3'),
]
