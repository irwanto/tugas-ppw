from django.db import models

# Create your models here.
#judul, isi, author, image
class News(models.Model): 
    photo = models.URLField(null=True, blank=True, default='')
    title = models.CharField(max_length=100)
    content = models.TextField()
    baca = models.URLField(null=True, blank=True, default='')
