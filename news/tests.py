from django.test import TestCase
from .views import *
from django.test import Client
from django.urls import resolve
import unittest

# Create your tests here.
class NewsTest(TestCase):
	def test_news_url_is_exist(self):
		response = Client().get('/news/')
		self.assertEqual(response.status_code,200)

	def test_news_using_landing_page_function(self):
		found = resolve('/news/')
		self.assertEqual(found.func, indexNews)

	def test_news_using_landing_page_template(self):
		response = Client().get('/news/')
		self.assertTemplateUsed(response,'news.html')

    # def test_html_contain_brand(self):
	# 	response = Client().get('/news/')
	# 	html_response = response.content.decode('utf8')
	# 	self.assertIn('Berita',html_response)
        
    # def test_model_can_create_new_news(self):
    #     new_activity = News.objects.create(title='banjir')
    #     counting_all_available_news = News.objects.all().count()
    #     self.assertEqual(counting_all_available_news, 1)
    
    