from django.http import HttpResponseRedirect
from django.shortcuts import render
from .models import *

def indexNews(request):
    all_news = News.objects.all()
    return render(request, 'news.html', {'new': all_news})

def berita1(request):
    return render(request, 'berita1.html', {})

def berita2(request):
    return render(request, 'berita2.html', {})

def berita3(request):
    return render(request, 'berita3.html', {})