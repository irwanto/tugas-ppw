from django.apps import AppConfig


class DonaturConfig(AppConfig):
    name = 'Donatur'
