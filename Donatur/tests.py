from django.test import TestCase
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.apps import apps
from .apps import DonaturConfig
from .models import Donatur
from .views import registration, submit
from django.http import HttpRequest
import unittest

class StoryUnitTest(TestCase):
    def test_registrasi_page_is_exist(self):
        response = Client().get('/registrasi')
        self.assertEqual(response.status_code,200)

    def test_using_registration_func(self):
        found = resolve('/registrasi')
        self.assertEqual(found.func, registration)
        
    def test_apps(self):
        self.assertEqual(DonaturConfig.name, 'Donatur')
        self.assertEqual(apps.get_app_config('Donatur').name, 'Donatur')



# Create your tests here.

