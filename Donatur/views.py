from django.shortcuts import render
from .models import Donatur
from .forms import Donatur_Form
from django.http import HttpResponseRedirect
from django.contrib.auth.forms import UserCreationForm

def submit(request):
    form = Donatur_Form(request.POST)
    if request.method == 'POST' and form.is_valid():
        response = {}
        response['name'] = request.POST['name']
        response['date'] = request.POST['date']
        response['email'] = request.POST['email']
        response['password'] = request.POST['new_password']
        akun = Donatur(namadonatur=response['name'], tanggallahir = response['date'],
                          email=response['email'], password=response['password'])
        akun.save()
        return render(request, 'doneregis.html', response)
    else:
        return render(request, 'registration.html', {'regisform': form})

def registration(request):
    return render(request, "registration.html", {'regisform' : Donatur_Form})
