from django.urls import path
from .views import registration, submit

app_name='Donatur'
urlpatterns = [
    path('', registration, name='registrasi'),
    path('done/', submit, name='submit')
]
