from django import forms
from .models import Donatur

class Donatur_Form(forms.Form):
    attrsname = {
        'class' : 'form-control',
        'placeholder' : 'Masukan namamu'
    }

    attrsdate = {
        'class' : 'form-control',
        'placeholder' : 'Masukan tanggal lahirmu',
        'type' : 'date'
    }

    attrsemail = {
        'class' : 'form-control',
        'placeholder' : 'Masukan emailmu'
    }

    attrspassword = {
        'class' : 'form-control',
        'placeholder' : 'Masukan password',
        'type' : 'password'
    }



    name = forms.CharField(label='Nama', required=True, max_length=40, widget=forms.TextInput(attrs=attrsname))
    date = forms.DateTimeField(label='Tanggal Lahir', required=True, widget=forms.DateInput(attrs=attrsdate))
    email = forms.EmailField(label='Email', required=True, max_length=30, widget=forms.TextInput(attrs=attrsemail))
    new_password = forms.CharField(label='Password', required=True, max_length=30, widget=forms.TextInput(attrs=attrspassword))
    #confirm_password = forms.CharField(label='Konfirmasi Password', required=True, max_length=30, widget=forms.TextInput(attrs=attrspassword))

    #def clean_password(self):
    #    new_password = self.cleaned_data.ge("new_password")
    #    confirm_password = self.cleaned_data.get("confirm_password")
    #    if (new_password == confirm_password):
    #       return confirm_password
    #    else:
    #        raise forms.ValidationError("Password tidak sama")

    def clean_email(self):
        email = self.cleaned_data.get('email')
        try:
            match = Donatur.objects.get(email=email)
        except Donatur.DoesNotExist:
            return email
        raise forms.ValidationError('Email sudah terdaftar')
